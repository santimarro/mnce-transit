# MNCE-transit

## File Structure


## Running the OpenTripPlanner server

Requeriment: Java version 15 or lower.

In order to run the server you need to open the terminal at the directory mnce-transit/otp and run the following command:

`java -Xmx2G -jar otp.jar --router current --graphs graphs --server`

After this you should be able to access the frontend by typing localhost:8080 in your browser.

In case the server gives an error we recommend to create the graph again by running the following command:

`java -Xmx2G -jar otp.jar --build graphs/current`

and then run the server again with:

`java -Xmx2G -jar otp.jar --router current --graphs graphs --server`


## Updated GTFS data from nicecotedazur

[GTFS](http://opendata.nicecotedazur.org/data/dataset/export-quotidien-au-format-gtfs-du-reseau-de-transport-lignes-d-azur)


## How to plan trips and compute isochrones with OpenTripPlanner

Refer to example.py
