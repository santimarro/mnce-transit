import geopandas as gpd
import pandas as pd
import folium
import matplotlib.pyplot as plt
import os, time, requests, json
import openrouteservice as ors
from shapely.geometry import shape, mapping, Point
import argparse
import pickle

#with open("times_100to200hour_92.txt", "rb") as fp:
#    b = pickle.load(fp)
#    print(b)
#    exit()
#
with open("county_to_radcenter.txt", "rb") as fp:
    b = pickle.load(fp)
    #print(len(b)) 1212750

    

parser = argparse.ArgumentParser()
parser.add_argument('--start', type=int, default=0)
parser.add_argument('--end', type=int, default=len(b))
parser.add_argument('--timeofday', type=str, default='9')
args = parser.parse_args()
 

start=args.start
end=args.end
#timeofday=args.timeofday[0]+'%3A'+args.timeofday[1:]
timeofday=args.timeofday[0]+'%3A'+args.timeofday[1:]
timeofday = args.timeofday + ":00am"
print(timeofday)
b=b[start:end]

result_list=[]
for i in range(0,len(b)):
    dictionary={}
    #print(i)
    #print('from :', str(b[i]['lat']),str(b[i]['lon']),' to: ', str(b[i]['radlat']),str(b[i]['radlon']))
    #response = requests.get('http://localhost:8080/otp/routers/current/plan?fromPlace='+str(b[i]['lat'])+'%2C'+str(b[i]['lon'])+'&toPlace='+str(b[i]['radlat'])+'%2C'+str(b[i]['radlon'])+'&time='+timeofday+'&date=11-24-2021&mode=TRANSIT%2CWALK&maxWalkDistance=10000000000.672&arriveBy=false&wheelchair=false&locale=en&itinIndex=0')
    query_plan = {
        "toPlace": str(b[i]['radlat']) + "," + str(b[i]['radlon']), # latlong of place
        "fromPlace": str(b[i]['lat']) + "," + str(b[i]['lon']),  # latlong of place
        "arriveBy": "TRUE",
        "mode": "WALK,BUS,TRAM",  # modes we want the route planner to use
        "date": "11-25-2021",
        "time": timeofday,
        "maxWalkDistance": 10000000000,  # in metres
        "walkReluctance": 5,
        "minTransferTime": 120,  # in secs
    }
    response = requests.get(
        "http://localhost:8080/otp/routers/current/plan", params=query_plan
    )
    response = response.json()
    try:
        b[i]['timedist']=response['plan']['itineraries'][0]['duration']
        result_list.append(b[i].copy())
        del b[i]['timedist']
        #print(response['plan']['itineraries'][0]['duration'])
        
    except:
        b[i]['timedist']='None'
        result_list.append(b[i].copy())
        del b[i]['timedist']
        #print('None')
    if i%10==0:
        print(i)
        
import pickle
with open("times_"+str(start)+"to"+str(end)+'hour_'+args.timeofday+"2.txt", "wb") as fp:
    pickle.dump(result_list, fp)
