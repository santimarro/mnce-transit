import requests
import argparse

query_isochrone = {
    "arriveBy": "TRUE",
    "mode": "WALK,BUS,TRAM",  # modes we want the route planner to use
    "date": "11-25-2021",
    "time": "09:00am",
    "maxWalkDistance": 500,  # in metres
    "walkReluctance": 5,
    "minTransferTime": 120,  # in secs
    "cutoffSec": 600,
    "cutoffSec": 900,
    "cutoffSec": 1200,
    "cutoffSec": 1500,
}

query_plan = {
    "arriveBy": "TRUE",
    "mode": "WALK,BUS,TRAM",  # modes we want the route planner to use
    "date": "11-25-2021",
    "time": "09:00am",
    "maxWalkDistance": 500,  # in metres
    "walkReluctance": 5,
    "minTransferTime": 120,  # in secs
}


def get_plan(query):
    """
    Args:
        query (dict): Dictionary with the query parameters

    Returns:
        json: JSON response from the server
    """
    response = requests.get(
        "http://localhost:8080/otp/routers/current/plan", params=query
    )
    return response.json()


def get_isochrone(query):
    """
    Args:
        query (dict): Dictionary with the query parameters

    Returns:
        json: JSON response from the server
    """

    response = requests.get(
        "http://localhost:8080/otp/routers/current/isochrone",
        params=query,
    )
    return response.json()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("from_lat", type=str, help="latitude origin point")
    parser.add_argument("to_lat", type=str, help="latitude destination point")

    parser.add_argument("from_long", type=str, help="longitude origin point")
    parser.add_argument(
        "to_long", type=str, help="longitude destination point"
    )

    parser.add_argument("--plan", dest="plan", action="store_true")
    parser.add_argument("--isochrone", dest="isochrone", action="store_true")

    args = parser.parse_args()

    if args.plan:
        query_plan["fromPlace"] = args.from_lat + "," + args.from_long
        query_plan["toPlace"] = args.to_lat + "," + args.to_long
        print(get_plan(query_plan))
    elif args.isochrone:
        query_isochrone["fromPlace"] = args.from_lat + "," + args.from_long
        query_isochrone["toPlace"] = args.to_lat + "," + args.to_long
        print(get_isochrone(query_isochrone))
