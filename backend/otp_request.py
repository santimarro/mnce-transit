import requests
from time import time

#example url
#http://localhost:8080/otp/routers/current/plan?fromPlace=43.70341,7.26540&toPlace=43.69124,7.24793&date=11-29-2021&time=08:00am&mode=TRAM,WALK,BUS&cutoffSec=600

from_place = [43.70341, 7.26540]
to_place = [43.69124, 7.24793]
req_time = time()
r = requests.get("http://localhost:8080/otp/routers/current/plan",
                 {
                     "fromPlace": str(from_place[0])+","+str(from_place[1]),
                     "toPlace": str(to_place[0])+","+str(to_place[1]),
                     "arriveBy": True,
                     "mode": "WALK,BUS,TRAM",
                     "date": "11-25-2021",
                     "time": "09:00am",
                 }
                 )

print("request_time", time()-req_time)
print(r.json())
