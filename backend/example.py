# Example of how to use the API
from api import get_isochrone, get_plan
import numpy as np

# Origin point
from_lat = "43.71004"
from_long = "7.25801"

# Destination point
to_lat = "43.6960456318"
to_long = "7.27415437420"

# Get a plan

# First create dictionary with all parameters
query_plan = {
    "toPlace": (to_lat + "," + to_long,),  # latlong of place
    "fromPlace": (from_lat + "," + from_long,),  # latlong of place
    "arriveBy": "TRUE",
    "mode": "WALK,BUS,TRAM",  # modes we want the route planner to use
    "date": "11-25-2021",
    "time": "09:00am",
    "maxWalkDistance": 500,  # in metres
    "walkReluctance": 5,
    "minTransferTime": 120,  # in secs
}
# Run the function
plan = get_plan(query_plan)
print(plan)
# plan will be a json object with all the planning information.

# Get an isochrone
# Change origin point to be the same as the dest point
from_lat = to_lat
from_long = to_long
# First create dictionary with all parameters
cut_off_list = tuple(np.arange(60, 100 * 60, 60))
query_isochrone = {
    "toPlace": (to_lat + "," + to_long,),  # latlong of place
    "fromPlace": (from_lat + "," + from_long,),  # latlong of place
    "arriveBy": "TRUE",
    "mode": "WALK,BUS,TRAM",  # modes we want the route planner to use
    "date": "11-25-2021",
    "time": "09:00am",
    "maxWalkDistance": 500,  # in metres
    "walkReluctance": 5,
    "minTransferTime": 120,  # in secs
    "cutoffSec": cut_off_list,
}

# Run the function
isochrone = get_isochrone(query_isochrone)
print(isochrone)
# isochrone will be a json object with the polygons generated. Could be saved as geojson as well.
