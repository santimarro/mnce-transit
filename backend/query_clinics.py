# Get an isochrone
from backend.api import get_isochrone, get_plan
import os
import geopandas as gpd
import numpy as np
import json
# Change origin point to be the same as the dest point

radiologies_filename = os.path.join("data/radiologies_paca.geojson")
df_radiologies = gpd.read_file(radiologies_filename).to_numpy()
for radio_id, radio in enumerate(df_radiologies):
    pt = np.array(radio[-1])
    print(pt)
    from_lat = pt[1]
    from_long = pt[0]
    # First create dictionary with all parameters
    query_isochrone = {
        "toPlace": (str(pt[1]) + "," + str(pt[0])),  # latlong of place
        "fromPlace": (str(from_lat) + "," + str(from_long)),  # latlong of place
        "arriveBy": "TRUE",
        "mode": "WALK,BUS,TRAM",  # modes we want the route planner to use
        "date": "11-25-2021",
        "time": "09:00pm",
        "maxWalkDistance": 1500,  # in metres
        "walkReluctance": 5,
        "minTransferTime": 120,  # in secs
        "cutoffSec": tuple(np.arange(60, 100*60, 60))
    }

    # Run the function
    isochrone = get_isochrone(query_isochrone)
    print(isochrone)
    clinic_file_name = os.path.join("data", "clinics", "clinic_"+str(radio_id)+".json")
    with open(clinic_file_name, "w") as fp:
        json.dump(isochrone, fp, indent=4)
    #if radio_id == 10:
    #    exit()
