import geopandas as gpd
import pandas as pd
import folium
import matplotlib.pyplot as plt
import numpy as np
import os, time, requests, json
import openrouteservice as ors
from shapely.geometry import shape, mapping, Point
import argparse
import pickle
from descartes import PolygonPatch
patch_blue = "#B2CBE5"

iris_filename = os.path.join("data/iris/iris.shp")
df_iris = gpd.read_file(iris_filename).to_numpy()
fig, axes = plt.subplots(nrows=1, ncols=1)

with open("times_0to50000.txt", "rb") as fp:
    b = pickle.load(fp)
exit()

first_code = b[0]["code"]
print(first_code)
for iris_id, iris in enumerate(df_iris):
    if iris[3] == first_code:
        iris_poly = iris[-1]
        patch = PolygonPatch(iris[-1], fc=patch_blue, ec=patch_blue, alpha=0.5, zorder=0)
        axes.add_patch(patch)

density_points = []
for den in b:
    if den["code"] != first_code:
        break
    print(den)
    density_points.append([den["lon"], den["lat"]])
print(len(density_points))
print(len(b))

density_points = np.array(density_points)

axes.scatter(density_points[:, 0], density_points[:, 1])
axes.axis("equal")
axes.axis("off")
plt.show()
