import matplotlib.pyplot as plt
from shapely.validation import explain_validity
from scipy import stats
import json
import geopandas as gpd
from shapely.geometry import MultiPolygon, Polygon, GeometryCollection
import numpy as np
from descartes import PolygonPatch
patch_blue = "#B2CBE5"
import seaborn as sns
import os
import ast

iris_filename = os.path.join("data/iris/iris.shp")
df_iris = gpd.read_file(iris_filename).to_numpy()
density_filename = os.path.join("data/population.json")
with open(density_filename, "r") as fp:
    density = json.load(fp)
density = ast.literal_eval(density)
all_density_points = []
all_density_values = []

for iris_density in density:
    density_points = [np.array([den["lon"], den["lat"]])
                      for den in iris_density["density"]]
    density_values = [den["pop"] for den in iris_density["density"]]
    all_density_points += density_points
    all_density_values += density_values

all_density_points = np.array(all_density_points)
all_density_values = np.array(all_density_values)

print(len(all_density_points))
print(len(all_density_values))
kernel = stats.gaussian_kde(dataset=all_density_points.T, weights=all_density_values)
#z = np.reshape(kernel(all_density_points.T).T, all_density_points.shape[0])
xmin = np.min(all_density_points[:, 0])
xmax = np.max(all_density_points[:, 0])
ymin = np.min(all_density_points[:, 1])
ymax = np.max(all_density_points[:, 1])

X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
positions = np.vstack([X.ravel(), Y.ravel()])
#z = kernel(positions)
Z = np.reshape(kernel(positions).T, X.shape)
#z = np.reshape(z, 1)
#print(z.shape)

#fig, ax = plt.subplots()
#ax.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r,
#          extent=[xmin, xmax, ymin, ymax])

#plt.show()
#exit()
## plot solution
fig, axes = plt.subplots(nrows=1, ncols=1)
for iris_id, iris in enumerate(df_iris):
    iris_density = None
    iris_poly = iris[-1]
    patch = PolygonPatch(iris[-1], fc=patch_blue, ec=patch_blue, alpha=0.5, zorder=0)
    axes.add_patch(patch)
    #print(sample_dict[iris_id]["sample_centers"])

iso_polys = []

#for clinic_file_name in ["clinics/clinic_0.json"]:#, "clinics/clinic1.geojson"]:
#for clinic_file_name in ["clinics/clinic_0.json", "clinics/clinic_1.json", "clinics/clinic_2.json"]:#, "clinics/clinic1.geojson"]:

for clinic_file_name in ["data/clinics/100_9pm/clinic_"+str(i)+".json" for i in range(49)]:#, "clinics/clinic1.geojson"]:
#for clinic_file_name in ["clinics/100/clinic_"+str(i)+".json" for i in [0, 20]]:#, "clinics/clinic1.geojson"]:
    with open(clinic_file_name, "r") as fp:
        clinic_0 = json.load(fp)
    #print(clinic_0)

    isochrones = clinic_0["features"]
    print(len(isochrones))
    for iso_id, isochrone in enumerate(isochrones):
        if isochrone["geometry"] is None:
            continue
        #print(isochrone["geometry"])
        mp = isochrone["geometry"]["coordinates"]
        polys = []
        #print(mp)
        for m in mp:
            #print(m)
            for tmp_m in m:
                new_tmp_m = np.array(tmp_m, dtype=float)
                #print(new_tmp_m)
                #axes.plot(m[:, 0], m[:, 1])
                poly = Polygon(new_tmp_m)
                polys.append(poly)
            #patch = PolygonPatch(poly, fc="red", ec=patch_blue, alpha=0.5, zorder=0)
            #axes.add_patch(patch)
        #break
        #print(len(mp))
        #print(iso_cmap[iso_id])
        #patch = PolygonPatch(MultiPolygon(polys), fc=iso_cmap[iso_id], ec=patch_blue, alpha=0.5, zorder=iso_id)
        #axes.add_patch(patch)
        #print(explain_validity(MultiPolygon(polys)))
        iso_polys.append({
            "poly": MultiPolygon(polys),#.buffer(0),
            "time": isochrone["properties"]["time"]
        })
    #break

#for i, iso_poly in enumerate(iso_polys):
#    #print(iso_poly["poly"].is_valid)
#    for other_i, other_iso_poly in enumerate(iso_polys):
#        if i == other_i:
#            continue
#        if iso_poly["time"] > other_iso_poly["time"]:
#            print(iso_poly["time"], other_iso_poly["time"])
#            if iso_poly["poly"].intersects(other_iso_poly["poly"]):
#                #print(i, other_i)
#                #print(iso_poly["poly"])
#                #print(other_iso_poly["poly"])
#                diff = iso_poly["poly"].difference(other_iso_poly["poly"])
#                if type(diff) == GeometryCollection:
#                    for tmp_diff in diff:
#                        if type(tmp_diff) == Polygon or type(tmp_diff) == MultiPolygon:
#                            diff = tmp_diff
#                iso_polys[i]["poly"] = diff

times = [iso_poly["time"] for iso_poly in iso_polys]
min_time = np.min(times)
max_time = np.max(times)
iso_cmap = sns.color_palette("mako", n_colors=len(iso_polys))
iso_cmap = sns.color_palette("mako", as_cmap=True)
iso_cmap = sns.color_palette("crest", as_cmap=True)
print(len(iso_polys))

#sort by time
time_layers = {}
time_sort_arg = reversed(np.argsort(times))
iso_polys = [iso_polys[time_sort_id] for time_sort_id in time_sort_arg]

for iso_id, iso_poly in enumerate(iso_polys):
    #patch = PolygonPatch(iso_poly["poly"], fc=iso_cmap[iso_id], ec=patch_blue, alpha=0.5, zorder=iso_id)
    #print((iso_poly["time"]-min_time)/(max_time-min_time))
    #patch = PolygonPatch(iso_poly["poly"], fc=iso_cmap((iso_poly["time"]-min_time)/(max_time-min_time)), ec=patch_blue, alpha=0.5, zorder=iso_id)
    patch = PolygonPatch(iso_poly["poly"], fc=iso_cmap((iso_poly["time"]-min_time)/(max_time-min_time)), ec="#00000000", zorder=iso_id)
    #patch = PolygonPatch(iso_poly["poly"], c=(iso_poly["time"]-min_time)/(max_time-min_time), ec="#00000000", zorder=iso_id, cmap=iso_cmap)
    #patch = PolygonPatch(iso_poly["poly"], fc="red", ec="#00000000", zorder=iso_id)
    axes.add_patch(patch)
    #if iso_id == 10:
    #    break

density_cmap = sns.color_palette("magma", as_cmap=True)
point_orders = np.argsort(all_density_values)
#m = axes.scatter(all_density_points[:, 0], all_density_points[:, 1], c=np.clip(all_density_values, a_min=0, a_max=100), zorder=500, cmap=density_cmap)
#m = axes.scatter(all_density_points[point_orders, 0], all_density_points[point_orders, 1],
#                 c=np.clip(all_density_values[point_orders], a_min=0, a_max=100), zorder=500, cmap=density_cmap)
#axes.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r)
#            #extent=[xmin, xmax, ymin, ymax])
#sm = plt.cm.ScalarMappable(cmap=density_cmap, norm=plt.Normalize(vmin=np.min(all_density_values), vmax=np.max(all_density_values)))
sm = plt.cm.ScalarMappable(cmap=density_cmap, norm=plt.Normalize(vmin=np.min(all_density_values), vmax=100))
sm = plt.cm.ScalarMappable(cmap=iso_cmap, norm=plt.Normalize(vmin=np.min(times), vmax=np.max(times)))
sm.set_array([])
#plt.title("Population density")
#cb = plt.colorbar(sm)
#label = cb.set_label("population density")


axes.axis("equal")
axes.axis("off")
fig.set_size_inches((10, 10))
plt.savefig("output/iso_100_min_4pm.png", transparent=True, dpi=500)
#plt.show()