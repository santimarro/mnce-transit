import geopandas as gpd
import pickle
from scipy import stats
from scipy.spatial import KDTree
import pandas as pd
import folium
import os, time, requests, json
from shapely.geometry import shape, mapping, Point
import openrouteservice as ors
import matplotlib.pyplot as plt
import json
import ast
import numpy as np
from descartes import PolygonPatch
from gurobi_model import find_clusters, integrated_sphere, find_minimal_sphere, precompute_spheres, gurobi_spheres
import seaborn as sns

step_dist = 0.0008330000000000837

patch_blue = "#B2CBE5"
# filenames
radiologies_filename = os.path.join("data/radiologies_paca.geojson")
iris_filename = os.path.join("data/iris/iris.shp")
transport_folder = os.path.join("data/transport")
density_filename = os.path.join("data/population.json")

def compute_iris_samples(iris_id):
    # Simple iteration using geopandas
    #df_radiologies = gpd.read_file(radiologies_filename)
    #print(df_radiologies.head())
    with open(density_filename, "r") as fp:
        density = json.load(fp)
    density = ast.literal_eval(density)

    df_iris = gpd.read_file(iris_filename).to_numpy()
    #fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
    #                    bottom=0.0,
    #                    top=1.0)
    #

    # prepare minimum densities
    all_density_values = []
    summed_densities = []
    for iris in df_iris:
        iris_density = None
        for tmp_density in density:
            if tmp_density["code"] == iris[3]:
                iris_density = tmp_density
                break
        density_values = [den["pop"] for den in iris_density["density"]]
        all_density_values.append(density_values)
        summed_densities.append(np.sum(density_values))
    #print(len(all_density_values))
    #print(summed_densities)
    min_density = np.min(summed_densities)
    median_density = np.median(summed_densities)
    total_density = np.sum(summed_densities)
    print("total_density", total_density)
    total_comp_budget = 2000
    #print(all_density_values)
    all_density_values = []
    #plt.hist(summed_densities, bins=len(all_density_values))
    #plt.show()
    #exit()

    all_density_points = []

    nb_clusters = 10
    VERBOSE = True

    sample_dict = []
    iris = df_iris[iris_id]
    #for iris_id, iris in enumerate(df_iris[8:]):
    print("iris_id", iris_id)
    #print(iris)
    iris_density = None
    for tmp_density in density:
        if tmp_density["code"] == iris[3]:
            iris_density = tmp_density
            break
    #print(iris_density)
    iris_poly = iris[-1]
    nb_clusters = int(np.ceil(total_comp_budget*(summed_densities[iris_id]/total_density)))
    if nb_clusters == 0:
        tmp_sample_dict = {"code": iris[3],
                           "sample_centers": [],
                           "sample_radii": []}
        with open("../sample_dict_"+str(iris_id)+".json", "w") as fp:
            json.dump(tmp_sample_dict, fp, indent=4)
        return

    if VERBOSE:
        print("nb_clusters", nb_clusters)

    density_points = [np.array([den["lon"], den["lat"]])
                               for den in iris_density["density"]]
    density_values = [den["pop"] for den in iris_density["density"]]
    total_population = np.sum(density_values)
    all_density_points += density_points
    all_density_values += density_values
    max_coverage = len(density_points)

    #for i in range(len(density_points)):
    if VERBOSE:
        fig, axes = plt.subplots(nrows=1, ncols=1)
        # plot solution
        patch = PolygonPatch(iris[-1], fc=patch_blue, ec=patch_blue, alpha=0.5, zorder=0)
        axes.add_patch(patch)
        density_cmap = sns.color_palette("magma", as_cmap=True)
        m = axes.scatter(np.array(density_points)[:, 0], np.array(density_points)[:, 1],
                         c=np.array(density_values), cmap=density_cmap)
        cb = fig.colorbar(m)
        xmin = np.min(np.array(iris[-1].exterior.coords)[:, 0])
        xmax = np.max(np.array(iris[-1].exterior.coords)[:, 0])
        ymin = np.min(np.array(iris[-1].exterior.coords)[:, 1])
        ymax = np.max(np.array(iris[-1].exterior.coords)[:, 1])
        #X, Y = np.mgrid[xmin:xmax:0.1j, ymin:ymax:0.1j]
        #kernel = stats.gaussian_kde(np.array(density_points).T)
        #print(kernel(np.array(density_points).T))

        #z = np.reshape(kernel(np.array(density_points).T), len(density_points))
        #axes.imshow(z, cmap=plt.cm.gist_earth_r,
        #            extent=[np.min(density_values), np.max(density_values)])
        axes.axis("equal")
    #center = np.array(density_points[i])
    #radius, sphere_stencil = find_minimal_sphere(center, step_dist,
    #                                     np.array(density_points), np.array(density_values),
    #                                     target_population_size)
    #axes.scatter([center[0]], [center[1]], zorder=1, color="red")
    #draw_circle = plt.Circle((center[0], center[1]), radius=radius, color="red", alpha=0.3)
    #axes.add_artist(draw_circle)
    #for c_id, c in enumerate(centers):
    #    print(c_id)
    #    draw_circle = plt.Circle((c[0], c[1]), radius=radii[c_id], color=cluster_cmap[c_id], alpha=0.3, zorder=0)
    #    axes.add_artist(draw_circle)

    tmp_coverage = 0.0
    target_population_size = 10
    while_iter = 0
    while tmp_coverage < 0.8*max_coverage:
        target_population_size += 0.1*total_population
        stencils, centers, radii = precompute_spheres(np.array(density_points), np.array(density_values),
                                                      step_dist, target_population_size)
        cluster_cmap = sns.color_palette("Set1", n_colors=len(centers))
        for c_id, c in enumerate(centers):
            print(c_id)
            draw_circle = plt.Circle((c[0], c[1]), radius=radii[c_id], color=cluster_cmap[c_id], alpha=0.3, zorder=0)
            axes.add_artist(draw_circle)
        axes.axis("equal")
        axes.axis("off")
        fig.set_size_inches((10, 10))
        #plt.savefig("no_circles_"+str(selected_iris_id)+".png", transparent=True, dpi=500)
        plt.show()
        if VERBOSE:
            print("gurobi_spheres")
        tmp_coverage, stencil_ids = gurobi_spheres(stencils, len(density_points), nb_spheres=nb_clusters)
        if VERBOSE:
            print("coverage", tmp_coverage, max_coverage)
            print("target_pop_size", target_population_size)
        if while_iter > 20:
            break
        while_iter += 1

    #print("nb_stencils", len(stencil_ids))
    if VERBOSE:
        for vec_id, stencil_id in enumerate(stencil_ids):
            #print(stencil_id)
            draw_circle = plt.Circle((centers[stencil_id][0], centers[stencil_id][1]),
                                     radius=radii[stencil_id], color=cluster_cmap[vec_id], alpha=0.3, zorder=0)
            #axes.scatter(np.array(density_points)[stencils[stencil_id], 0],
            #             np.array(density_points)[stencils[stencil_id], 1],
            #             color=cluster_cmap[vec_id])
            axes.add_artist(draw_circle)

    #integral = np.sum(np.array(density_values)[sphere_stencil])
    #print("integral", integral)
    #integral, pts_in_sphere = integrated_sphere(np.array(density_points[0]), 2*step_dist+step_dist/2, density_points, density_values)
    #print(pts_in_sphere)
    if VERBOSE:
        plt.show()
    sample_dict.append(
        {"code": iris[3],
         "sample_centers": [centers[stencil_id].tolist() for stencil_id in stencil_ids],
         "sample_radii": [radii[stencil_id] for stencil_id in stencil_ids]})
    #print(density_points)

    tmp_sample_dict = {"code": iris[3],
                       "sample_centers": [centers[stencil_id].tolist() for stencil_id in stencil_ids],
                       "sample_radii": [radii[stencil_id] for stencil_id in stencil_ids]}
    with open("../sample_dict_"+str(iris_id)+".json", "w") as fp:
        json.dump(tmp_sample_dict, fp, indent=4)

if __name__ == "__main__":

    iris_codes = [
        '060883202',
        '061570102',
        '060660000',
        '060882504',
        '061570105',
        '061170000',
        '061230105',
        '060882906',
        '060881903',
        '061230109',
        '060881304',
        '060883101',
        '061260000',
        '060882805',
        '060881703',
        '060882705',
        '061230104',
        '061460000',
        '060881307',
        '060882801',
        '060881605',
        '060881401',
        '060881001',
        '060883201',
        '060881302',
        '060270107',
        '060880504',
        '060340000',
        '060881308',
        '060880206',
        '060650101',
        '060881603',
        '060882103',
        '060881203',
        '061230110',
        '060882104',
        '060880904',
        '060420000',
        '060270111',
        '060330103',
        '061230101',
        '060882201',
        '060881204',
        '060881005',
        '060881101',
        '060881804',
        '060881901',
        '060883602',
        '061190000',
        '060882704',
        '060881602',
        '060882602',
        '060881803',
        '060200000',
        '061230107',
        '060882601',
        '060881202',
        '060880701',
        '061230111',
        '060590000',
        '060270110',
        '060883001',
        '061440000',
        '060880404',
        '060882905',
        '061090000',
        '060881802',
        '060880304',
        '061230106',
        '060881309',
        '061490102',
        '060882303',
        '060882701',
        '060880605',
        '060600000',
        '060881902',
        '060883402',
        '060882202',
        '060640000',
        '061220000',
        '061570106',
        '060130000',
        '061490101',
        '060881502',
        '060881604',
        '060881606',
        '060330101',
        '061490103',
        '060270105',
        '060270102',
        '060460000',
        '060320000',
        '060881704',
        '060270108',
        '060882001',
        '061570104',
        '060250000',
        '060883601',
        '060880202',
        '060750000',
        '060880507',
        '060880505',
        '060270115',
        '060270116',
        '060880302',
        '060880601',
        '060880501',
        '061210000',
        '060883701',
        '060880103',
        '060881303',
        '060882903',
        '060883002',
        '061100000',
        '060880401',
        '060270117',
        '060883003',
        '060882603',
        '060880903',
        '060270101',
        '060880602',
        '060882904',
        '060880101',
        '060881404',
        '061470000',
        '060881103',
        '060650102',
        '061530000',
        '060881506',
        '061570107',
        '061590102',
        '060881003',
        '060882703',
        '061510000',
        '060800000',
        '060881801',
        '060880303',
        '060270103',
        '061590103',
        '060881002',
        '060881601',
        '060330102',
        '060730000',
        '060882203',
        '060880301',
        '060881306',
        '060881402',
        '061270000',
        '061020000',
        '060882802',
        '060882402',
        '060881503',
        '060882503',
        '060882902',
        '060881403',
        '060110000',
        '060882002',
        '060880402',
        '060881301',
        '061570103',
        '060882101',
        '060881701',
        '060880801',
        '060883102',
        '060880902',
        '060880502',
        '060270113',
        '060883801',
        '060881805',
        '061230103',
        '060881104',
        '060880503',
        '060880403',
        '060740000',
        '060882302',
        '060270106',
        '060881702',
        '060883403',
        '060882901',
        '060880305',
        '060882702',
        '060881705',
        '060270109',
        '060881305',
        '061230108',
        '060720000',
        '060883301',
        '060880508',
        '060880509',
        '060882804',
        '060270114',
        '060210000',
        '060882502',
        '060881201',
        '060880102',
        '060883401',
        '061200000',
        '060090000',
        '061590101',
        '060880604',
        '060883501',
        '060880203',
        '061570101',
        '061560000',
        '060550000',
        '060880205',
        '060882304',
        '060882301',
        '060880905',
        '060882401',
        '060330104',
        '060270104',
        '060880506',
        '060881505',
        '060882102',
        '060060000',
        '060882803',
        '061230102',
        '060882501',
        '061490104',
        '060881004',
        '061030000',
        '060880901',
        '061290000',
        '060330105',
        '060270112',
        '061140000',
        '060880603',
        '060880204',
        '060881501',
        '060881504',
        '060880702',
        '060881105',
        '060880201',
        '061110000',
        '060881102'
    ]
    iris_means = [

        1364.094342906875,
        574.2397502952585,
        3071.674030658251,
        357.8509818094206,
        800.2873245916714,
        1578.0219819391634,
        840.9726720647773,
        1256.38664778277,
        384.3639865877148,
        842.9544266720272,
        418.37200165425975,
        1256.7503170577045,
        2369.0177036256764,
        883.2194439210829,
        243.20000000000002,
        823.2443697784236,
        1014.1157739022111,
        2912.7174946004307,
        752.5433954123822,
        732.0158997988189,
        626.1241540950055,
        374.69260100600366,
        747.9154720711814,
        1081.4005244070624,
        310.1473868559314,
        572.6588572267921,
        425.9737781804616,
        2006.3609802073515,
        804.8462843569732,
        278.51069717935076,
        1416.5127818936724,
        362.5835169566648,
        816.9780738946095,
        744.0074214432339,
        1437.3874019433065,
        438.5631371482942,
        237.64865491651207,
        1452.623348017622,
        917.86583289296,
        330.717125382263,
        1249.586137581894,
        325.8911111111112,
        721.2196466431095,
        898.6629746287769,
        404.001377214228,
        361.8101146165662,
        393.37608509597754,
        1029.1952017958188,
        11478.602502406162,
        506.5745666112562,
        362.2627410192817,
        353.45632245888385,
        340.7204157386785,
        4228.106070369894,
        585.5249547982121,
        520.7090322580644,
        449.4674154674155,
        305.1353106094142,
        747.1293931293932,
        3406.7061522286313,
        910.2047882754375,
        1313.5729361107176,
        2838.617921146953,
        935.5977504017144,
        544.7949258077508,
        2453.1314068761867,
        446.87693863789605,
        274.2871455255561,
        739.3375418150425,
        637.560421994885,
        1250.23840969433,
        461.3019681349578,
        539.4937456297102,
        625.0721435133697,
        1559.4637178375249,
        455.78886369362556,
        840.1697211155376,
        217.05349821896633,
        1179.3712481825976,
        1392.1309927962209,
        609.3643093707382,
        3619.4084710201932,
        1022.0735785953175,
        414.9547689282203,
        465.4728331341689,
        709.9587148039386,
        656.758434894163,
        989.7712862318839,
        998.3690639269407,
        653.8754032752072,
        2183.263654432968,
        3942.573312185896,
        208.29706825469538,
        1004.9494770580295,
        560.5064703257475,
        904.5334346504578,
        2288.5022128741048,
        1139.321782660989,
        479.97581395348845,
        2890.84931947272,
        463.0845157310304,
        230.38917975567193,
        999.8680441551091,
        971.4304998663457,
        535.2261715404381,
        287.61558160710223,
        542.001052631579,
        2747.424358974361,
        1295.5288731950302,
        393.2020004348773,
        278.51596821934885,
        1179.5086012789334,
        698.3087796903548,
        9809.81947743468,
        645.7807546327306,
        814.942490599425,
        728.9697771587744,
        404.27311651635233,
        1422.0894385889442,
        380.93764805928436,
        298.9668785138109,
        870.975625332624,
        346.172116119175,
        565.1029750391451,
        2266.9011934968544,
        354.8353817823647,
        1576.9154572584914,
        7516.055321995039,
        178.0982086934736,
        1002.6655608123699,
        1275.1844752324914,
        673.3602250165452,
        866.237704918033,
        3929.6506059710314,
        4837.868209255533,
        346.8722196743866,
        338.48142125376046,
        675.7719425887958,
        1957.1293644007408,
        538.3690155213877,
        384.28538966503953,
        511.8044658086526,
        8791.757353669387,
        240.55395306911691,
        452.1774499473129,
        604.3360655737705,
        285.76567505720817,
        5956.319505973951,
        7669.70844212359,
        1109.4245817851972,
        371.14342179559566,
        276.968455063345,
        165.93195673549658,
        983.0961051204998,
        406.5305476172086,
        1900.030687510322,
        551.9262608695653,
        632.3653523795641,
        450.7195863746959,
        909.8961684180427,
        613.0983915806196,
        269.0810963181404,
        1580.4674695853848,
        1428.1843922061594,
        334.32440822662016,
        423.887991927346,
        1265.1402726146218,
        1131.436633933005,
        281.8871074850584,
        887.8384847342634,
        351.6139484199055,
        338.6141826923077,
        669.4961493582261,
        1824.06018916595,
        464.71954821894,
        1678.0985651001547,
        208.9211677293565,
        1233.1077376023554,
        1088.011601939683,
        357.69088652971186,
        497.2362397459644,
        230.49402501157942,
        982.0205511885597,
        523.2473018809744,
        560.4425652556454,
        3447.940047961631,
        1869.4540698928,
        657.5630200308165,
        726.7082767978291,
        637.6490145456836,
        893.2693448243907,
        2708.404416461356,
        322.40765361121095,
        770.0676113360323,
        303.5692599620494,
        1026.064455475331,
        6299.111838414062,
        2823.998974358976,
        1309.4281591165782,
        259.65584714548805,
        1696.6084570589169,
        174.97003777472526,
        369.7343904856294,
        7205.796296296296,
        6689.2097759674125,
        184.82506565509198,
        279.8719818505914,
        436.54038787422326,
        484.9978760378452,
        334.1966655324942,
        1347.8954638359592,
        981.384125271553,
        295.9921507064365,
        173.84756960830586,
        395.7281204468188,
        1716.9426195426186,
        839.3789833080426,
        1021.9976697219208,
        214.87323752711495,
        2722.6715123634303,
        749.9145368827765,
        716.5699570815451,
        617.3686685962372,
        4242.076668671075,
        1135.0387790743841,
        1733.804584782802,
        1557.8193145068121,
        294.97887158619614,
        192.181962925054,
        353.8847654391474,
        309.4117344841759,
        515.6938427970837,
        503.2788146279949,
        200.64461689143377,
        5471.432347826088,
        429.80496438404066,
    ]
    #import argparse
    #parser = argparse.ArgumentParser()
    #parser.add_argument("--iris_id", default=0, type=int, help="iris_id")
    #args = parser.parse_args()
    ##for i in range(11):
    #compute_iris_samples(args.iris_id)
    #compute_iris_samples(10)
    #exit()
    #files = [os.path.join("transfer_2458764_files_18649cda/times_"+str(i)+"to"+str(i+50000)+".txt") for i in range(0, 1150000, 50000)]
    #print(files)
    #unreachable_points = []
    #for file in files:
    #    print(file)
    #    #file = "times_0to50000.txt"
    #    with open(file, "rb") as fp:
    #        b = pickle.load(fp)

    #    first_pos = np.array([b[0]["lat"], b[0]["lon"]])
    #    time_dists = []

    #    for den in b:
    #        pos = np.array([den["lat"], den["lon"]])
    #        if not np.isclose(np.linalg.norm(first_pos - pos), 0.0, atol=1e-6):
    #            if len(time_dists) == 0:
    #                unreachable_points.append([first_pos[1], first_pos[0]])
    #            first_pos = pos
    #            time_dists = []
    #        elif not den["timedist"] == "None":
    #            time_dists.append(den["timedist"])
    #        #if den["timedist"] == "None":
    #        #    print(den)
    #            #exit()
    #    #break
    #    print(len(unreachable_points))

    #print(unreachable_points)
    selected_iris_id = 22
    #for selected_iris_id in [10]:
    fig, axes = plt.subplots(nrows=1, ncols=1)
    density_cmap = sns.color_palette("magma", as_cmap=True)
    with open(density_filename, "r") as fp:
        density = json.load(fp)
    density = ast.literal_eval(density)
    df_iris = gpd.read_file(iris_filename).to_numpy()
    all_density_values = []
    all_density_points = []
    summed_densities = []
    min_time = np.min(iris_means)
    max_time = np.max(iris_means)
    for iris_id, iris in enumerate(df_iris):
        #if iris_id != selected_iris_id:
        #    continue
        iris_density = None
        for tmp_density in density:
            if tmp_density["code"] == iris[3]:
                iris_density = tmp_density
                break
        density_values = [den["pop"] for den in iris_density["density"]]
        density_points = [np.array([den["lon"], den["lat"]])
                          for den in iris_density["density"]]
        #m = axes.scatter(np.array(density_points)[:, 0], np.array(density_points)[:, 1],
        #             c=density_values, cmap=density_cmap)
        #cb = fig.colorbar(m)
        all_density_points += density_points
        all_density_values += density_values
    all_density_points = np.array(all_density_points)

    ## plot solution
    all_centers = []
    #cluster_cmap = sns.color_palette("magma", as_cmap=True)
    cm = plt.get_cmap('viridis')
    for iris_id, iris in enumerate(df_iris):
        #if iris_id != selected_iris_id:
        #    continue
        iris_density = None
        iris_poly = iris[-1]
        iris_mean = np.array(iris_means)[np.array(iris_codes) == iris[3]]
        mean_color = cm((iris_mean - min_time)/(max_time-min_time))
        #patch = PolygonPatch(iris[-1], fc=patch_blue, ec=patch_blue, alpha=0.5, zorder=0)
        patch = PolygonPatch(iris[-1], fc=mean_color, ec=mean_color, alpha=1.0, zorder=0)
        axes.add_patch(patch)
        #print(sample_dict[iris_id]["sample_centers"])
        with open("iris/sampling/sample_dict_"+str(iris_id)+".json", "r") as fp:
            sample_dict = json.load(fp)

        centers = sample_dict["sample_centers"]
        radii = sample_dict["sample_radii"]
        #cluster_cmap = sns.color_palette("Set1", n_colors=len(centers))
        all_centers += centers
        #for c_id, c in enumerate(centers):
        #    draw_circle = plt.Circle((c[0], c[1]),
        #                             radius=radii[c_id], color=cluster_cmap[c_id], alpha=0.5, zorder=10)
        #    #axes.add_artist(draw_circle)
    #print(len(all_centers))
    #m = axes.scatter(all_density_points[:, 0], all_density_points[:, 1], c=all_density_values, zorder=5)
    ##m = axes.scatter(all_density_points[:, 0], all_density_points[:, 1], zorder=10)
    #print(len(all_density_points))
    #cb = fig.colorbar(m)
    #cb.set_label("population_density")
    axes.axis("equal")
    axes.axis("off")
    fig.set_size_inches((10, 10))
    #axes.scatter(np.array(unreachable_points)[:, 0], np.array(unreachable_points)[:, 1], c="red", zorder=10)
    #plt.savefig("no_circles_"+str(selected_iris_id)+".png", transparent=True, dpi=500)
    sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=min_time/60, vmax=max_time/60))
    sm.set_array([])
    #plt.title("Population density")
    cb = plt.colorbar(sm)
    label = cb.set_label("minutes")
    #plt.show()
    fig.set_size_inches((10, 10))
    plt.savefig("output/iris_mean_times.png", transparent=True, dpi=500)

#all_density_points = np.array(all_density_points)
#all_density_values = np.array(all_density_values)
#
