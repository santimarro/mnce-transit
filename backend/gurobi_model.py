import gurobipy as gp
from gurobipy import GRB
import numpy as np

def find_clusters_spheres(stenctils, nb_spheres):
    return 0

def find_clusters(density_points, density_values, nb_clusters, population_size=80):
    m = gp.Model("find_clusters_problem")
    #m.setParam('OutputFlag', 0)
    cluster_indices = [str(i)+"_"+str(coord) for i in range(nb_clusters) for coord in range(2)]
    print(cluster_indices)
    cluster_positions = m.addVars(cluster_indices, vtype=GRB.CONTINUOUS, name="cluster_positions")
    #print(cluster_indices)

    print(density_values)
    per_point_cluster_label_indices = [str(i) for i in range(len(density_points))]
    print(per_point_cluster_label_indices)
    per_point_weights = {}
    for p_id in per_point_cluster_label_indices:
        per_point_weights[p_id] = density_values[int(p_id)]

    all_cluster_labels = []
    # cluster_labels
    for c_id in range(nb_clusters):
        all_cluster_labels.append(
            m.addVars(per_point_cluster_label_indices, vtype=GRB.BINARY, name="per_point_cluster_labels_"+str(c_id)))

    # area variables
    all_area_variables = []
    for c_id in range(nb_clusters):
        all_area_variables.append(
            m.addVar(lb=0.0, vtype=GRB.CONTINUOUS, name="area_variable_"+str(c_id)))

    # per_point_cluster_distances
    all_point_cluster_distances = []
    for c_id in range(nb_clusters):
        all_point_cluster_distances.append(
            m.addVars(per_point_cluster_label_indices, vtype=GRB.CONTINUOUS, name="per_point_cluster_distances_"+str(c_id), lb=0.0))

    m.update()
    # area constraints
    for c_id in range(nb_clusters):
        print(all_area_variables[c_id] >= population_size - all_cluster_labels[c_id].prod(per_point_weights))
        m.addConstr(all_area_variables[c_id] >= population_size - all_cluster_labels[c_id].prod(per_point_weights))
        m.addConstr(all_area_variables[c_id] >= -(population_size - all_cluster_labels[c_id].prod(per_point_weights)))

    # only one label per point
    for p_id in per_point_cluster_label_indices:
        m.addConstr(gp.quicksum([all_cluster_labels[c_id][p_id] for c_id in range(nb_clusters)]) == 1)

    m.update()
    # per_point_cluster_distances constraints
    # distance must be larger than the actual distances
    for c_id in range(nb_clusters):
        for p_id in per_point_cluster_label_indices:
            #print(p_id, c_id)
            #m.addConstr(all_point_cluster_distances[c_id].sum(p_id) >= 0.001)
#                        print(all_point_cluster_distances[c_id].sum(p_id) >=
#                  (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")) + \
#                  (density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1"))*(density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1")))
            #print(gp.QuadExpr(all_point_cluster_distances[c_id].sum(p_id)) >= \
            #            (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")))
            m.addConstr(all_point_cluster_distances[c_id].sum(p_id) == \
                        (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")) + \
                        (density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1"))*(density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1")))
                #m.addConstr(all_point_cluster_distances[c_id].sum(p_id) >= \
            #            (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")))#*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")))
            #m.addConstr(all_point_cluster_distances[c_id].sum(p_id) >= \
            #            -(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")))#*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")))
            #m.addConstr(all_point_cluster_distances[c_id].sum(p_id) >= \
            #            -(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")))
            #exit()
           # m.addConstr(all_point_cluster_distances[c_id].sum(p_id) >= \
           #             (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0")) + \
           #             (density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1"))*(density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1")))
        #print(int(np.min(np.array(density_points)[:, 0])) <= cluster_positions.sum(str(c_id)+"_0"))
        m.addConstr(cluster_positions.sum(str(c_id)+"_0") >= np.min(np.array(density_points)[:, 0]))
        m.addConstr(cluster_positions.sum(str(c_id)+"_0") <= np.max(np.array(density_points)[:, 0]))

        m.addConstr(cluster_positions.sum(str(c_id)+"_1") >= np.min(np.array(density_points)[:, 1]))
        m.addConstr(cluster_positions.sum(str(c_id)+"_1") <= np.max(np.array(density_points)[:, 1]))

    for c_id in range(nb_clusters):
        for other_c_id in range(nb_clusters):
            if other_c_id == c_id:
                continue
            d = (cluster_positions.sum(str(other_c_id)+"_0") - cluster_positions.sum(str(c_id)+"_0"))*(cluster_positions.sum(str(other_c_id)+"_0") - cluster_positions.sum(str(c_id)+"_0"))
            m.addConstr(d >= 0.00005)

    for p_id in per_point_cluster_label_indices:
        chosen_position = gp.quicksum([all_cluster_labels[c_id].sum(p_id)*all_point_cluster_distances[c_id].sum(p_id) for c_id in range(nb_clusters)])
        for c_id in range(nb_clusters):
            d = all_point_cluster_distances[c_id][p_id]
            #m.addConstr((1-all_cluster_labels[c_id][p_id])*d >= chosen_position)
            #print(d >= chosen_position)
            m.addConstr(d >= chosen_position)
    m.update()
    m.setParam("MIPGap", 0.1)
    m.setParam("NonConvex", 2)

    obj = gp.LinExpr()
    # area term
    area_term = gp.LinExpr()
    for c_id in range(nb_clusters):
        area_term += all_area_variables[c_id]
    obj += area_term

#    # distance term
    distance_term = gp.LinExpr()
    for c_id in range(nb_clusters):
        for p_id in per_point_cluster_label_indices:
            distance_term += all_cluster_labels[c_id].sum(p_id)*all_point_cluster_distances[c_id].sum(p_id)
#        #obj += 10.0*all_point_cluster_distances[c_id].sum()
    obj += 10.0*distance_term

    m.setObjective(obj, sense=GRB.MINIMIZE)
    #m.Params.timeLimit = 10.0
    m.update()
    m.optimize()
    best_obj = m.getObjective().getValue()
    print("obj_value", best_obj)
    print("distance_term", distance_term.getValue())
    print(cluster_positions)

    print("all_area_variables")
    print(all_area_variables)
    for p_id in per_point_cluster_label_indices:
        chosen_position = gp.quicksum([all_cluster_labels[c_id].sum(p_id)*all_point_cluster_distances[c_id].sum(p_id) for c_id in range(nb_clusters)])
        print(p_id)
        #print(all_cluster_labels[0][p_id], all_cluster_labels[1][p_id], all_cluster_labels[2][p_id])
        print(all_cluster_labels[0][p_id])
        print(all_point_cluster_distances[0][p_id], all_point_cluster_distances[1][p_id], all_point_cluster_distances[2][p_id])
        print(chosen_position)
        print(p_id, chosen_position.getValue())
        for c_id in range(nb_clusters):
            #d = (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))*(density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))
            d = (density_points[int(p_id)][0] - cluster_positions.sum(str(c_id)+"_0"))
                #(density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1"))*(density_points[int(p_id)][1] - cluster_positions.sum(str(c_id)+"_1"))
            print(all_point_cluster_distances[c_id].sum(p_id).getValue(), d.getValue())
    #    #exit()
    #    #for c_id in range(nb_clusters):
    #    #    d = all_point_cluster_distances[c_id][p_id]
    #    #    #m.addConstr((1-all_cluster_labels[c_id][p_id])*d >= chosen_position)
    #    #    print(d >= chosen_position)
    #    #    m.addConstr(d >= chosen_position)

    # parse solution
    final_all_cluster_labels = []
    for c_id in range(nb_clusters):
        final_cluster_labels = []
        solution = m.getAttr('x', all_cluster_labels[c_id])
        for corr_id, corr in enumerate(solution):
            #print(corr, solution[corr])
            if not np.isclose(solution[corr], 1.0):
                continue
            final_cluster_labels.append(int(corr))
            #final_end_intersections.append(corr[-1])
        final_all_cluster_labels.append(final_cluster_labels)

        solution = m.getAttr('x', cluster_positions)
        final_cluster_positions = np.zeros([nb_clusters, 2])
        for corr_id, corr in enumerate(solution):
            #print(corr, solution[corr])
            final_cluster_positions[int(corr.split("_")[0]), int(corr.split("_")[1])] = solution[corr]
            #final_end_intersections.append(corr[-1])
    return final_all_cluster_labels, final_cluster_positions

def gurobi_spheres(stencils, nb_pts, nb_spheres):
    m = gp.Model("find_clusters_problem")
    #m.setParam('OutputFlag', 0)

    sphere_counter_ids = [i for i in range(nb_spheres)]
    #sphere_counter_vars = m.addVars(sphere_counter_ids, vtype=GRB.BINARY, name="sphere_counter")

    stencil_selections_ids = [i for i in range(len(stencils))]
    stencil_selections_vars = m.addVars(stencil_selections_ids, vtype=GRB.BINARY, name="stencil_selection")

    pt_ids = [i for i in range(nb_pts)]
    pt_selected_vars = m.addVars(pt_ids, vtype=GRB.BINARY, name="pt_selected")
    pt_overlap_vars = m.addVars(pt_ids, vtype=GRB.BINARY, name="pt_overlap")

    m.update()

    for p_id in pt_ids:
        m.addConstr(pt_selected_vars[p_id] <=
                    gp.quicksum([stencils[stencil_id][p_id] * stencil_selections_vars[stencil_id] for stencil_id in stencil_selections_ids]))

    for p_id in pt_ids:
        m.addConstr(gp.quicksum([stencils[stencil_id][p_id] * stencil_selections_vars[stencil_id] for stencil_id in stencil_selections_ids])
                    <= 1 + 1000*(pt_overlap_vars[p_id]))

    m.addConstr(stencil_selections_vars.sum() <= nb_spheres)

    obj = gp.LinExpr()
    obj += pt_selected_vars.sum()
    obj += nb_pts - pt_overlap_vars.sum()
    m.setObjective(obj, sense=GRB.MAXIMIZE)
    m.Params.timeLimit = 60.0
    m.setParam("MIPGap", 0.01)
    m.update()

    m.optimize()
    #best_obj = m.getObjective().getValue()
    #print("obj_value", best_obj)
    final_coverage = pt_selected_vars.sum().getValue()
    #print("coverage", final_coverage)
    #print("overlap", pt_overlap_vars.sum().getValue())
    solution = m.getAttr('x', stencil_selections_vars)
    final_stencil_ids = []
    for corr_id, corr in enumerate(solution):
        #print(corr, solution[corr])
        if not np.isclose(solution[corr], 1.0):
            continue
        final_stencil_ids.append(int(corr))
    return final_coverage, final_stencil_ids


def precompute_spheres(pts, weights, step_size, target_pop_size=80):
    stencils = []
    centers = []
    radii = []
    for pt in pts:
        radius, stencil = find_minimal_sphere(np.array(pt), step_size, pts, weights, target_pop_size)
        stencils.append(stencil)
        centers.append(np.array(pt))
        radii.append(radius)
    return stencils, centers, radii

def find_minimal_sphere(center, step_size, pts, weights, target_pop_size=80):
    radius = step_size/2
    integral, stencil = integrated_sphere(center, radius, pts, weights)

    not_changed_counter = 0
    new_radius = radius
    while integral < target_pop_size:
        new_radius += step_size
        new_integral, new_stencil = integrated_sphere(center, new_radius, pts, weights)
        if np.isclose(integral, new_integral, atol=1e-3):
            not_changed_counter += 1
        else:
            not_changed_counter = 0
            radius = new_radius
            stencil = new_stencil
            integral = new_integral
        if not_changed_counter == 2:
            break

    return radius, stencil

def integrated_sphere(center, radius, pts, weights):
    pts_in_sphere = np.linalg.norm(np.array(center) - np.array(pts), axis=-1) < radius
    return np.sum(np.array(weights)[pts_in_sphere]), pts_in_sphere